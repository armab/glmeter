##
# Ruby-based tool to fetch the remote website URL within specified time period and measure its response time
module Glmeter
  ##
  # Remote website URL to benchmark/instrument
  def self.url
    'https://gitlab.com'
  end

  ##
  # Aggregated time in seconds to fetch the URL and measure HTTP response times during that period
  def self.timeout
    5 * 60
  end
end
