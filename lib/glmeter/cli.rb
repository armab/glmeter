require 'glmeter'
require 'glmeter/stats'
require 'uri'
require 'net/https'
require 'timeout'
# TODO: Fully-fledged CLI
#require 'optparse'


##
# Glmeter Command Line application
class Glmeter::CLI

  ##
  # Run it!
  def self.run(*args)
    # 'Timeout' starts a thread that checks execution time
    # http://www.mikeperham.com/2015/05/08/timeout-rubys-most-dangerous-api/
    begin Timeout::timeout(Glmeter::timeout) do
      # TODO: Do we need to follow the redirects (???) https://ruby-doc.org/stdlib-2.6.1/libdoc/net/http/rdoc/Net/HTTP.html#class-Net::HTTP-label-Following+Redirection
      # Current benchmarking setup includes (divide in future):
      # 1) DNS Lookup
      # 2) TCP Connection
      # 3) TLS Handshake (if HTTPS)
      # 4) Server response time (response headers + response body)

      puts "Testing #{Glmeter::url} for #{Glmeter::timeout}s"

      uri = URI.parse(Glmeter::url)
      http = Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = true

      loop do
        Glmeter::Stats::measure do
          # TODO: Show debugging messages on CLI '--verbose' arg
          #puts "Fetching: #{Glmeter::url}"
          response = http.request(Net::HTTP::Get.new(uri.request_uri))
          print '.'
          # TODO: Catch Net::HTTP timeouts, errors and edge cases
          # TODO: Process response, calculate HTTP failurs and report that nicely
          #puts "Got #{Glmeter::url}: #{response.message} #{response.code}"
        end
      end
    end

    # Report measured HTTP stats within specified time period
    rescue Timeout::Error
      puts Glmeter::Stats::report
      return 0
    end
  end
end
