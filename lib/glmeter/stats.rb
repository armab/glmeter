require 'glmeter'

##
# Monkey-patch Time to add milliseconds format
class Time
  def to_milliseconds
    (self.to_f * 1000.0).to_i
  end
end

##
# Class for gathering, measuring and instrumenting website response times
class Glmeter::Stats
  # An Array of Hashes with the HTTP response results
  @results = []

  ##
  # Measure the code block runtime
  #
  # Returns Integer time in milliseconds
  def self.measure # :yield:
    r0 = Time.now
    yield
    response_time = Time.now.to_milliseconds - r0.to_milliseconds
    @results << {'time': response_time}
    response_time
  end

  ##
  # Number of executions measured
  def self.executions_n
    @results.count
  end

  ## TODO!
  # Number of failed executions
  def self.failed_n
  end

  ##
  # Find the slowest HTTP response
  def self.slowest
    @results.max { |a,b| a[:time] <=> b[:time] }
  end

  ##
  # Find the fastest HTTP response
  def self.fastest
   @results.min { |a,b| a[:time] <=> b[:time] }
  end

  ##
  # Return average HTTP response time
  def self.average
    @results.sum { |k,v| k[:time] } / @results.count
  end

  ##
  # Calculate percentile as a correct way to report distribution of values/latency
  #
  # percent - Should be expressed as 50, 95, 99.99, etc
  def self.percentile(percent)
    time_array = @results.map { |a| a[:time] }
    # TODO: Use the mean of two nearest array values in case when selected index is not a round number
    time_array.sort[((time_array.length * percent.to_f / 100).ceil) - 1]
  end

  ##
  # Pretty prints gathered stats as a human-readble report
  def self.report
    """
  Responses: #{self.executions_n}
  Best:    #{self.fastest[:time]}ms
  Worst:   #{self.slowest[:time]}ms
  Average: #{self.average}ms

  Latency Distribution
    50%    #{self.percentile(50)}ms
    75%    #{self.percentile(75)}ms
    90%    #{self.percentile(90)}ms
    99%    #{self.percentile(99)}ms
"""
  end

  def self.to_s
    print self.report
  end
end