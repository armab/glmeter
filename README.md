# glmeter

Ruby-based CLI tool to fetch the remote website URL within specified time period and measure its response time.

## Usage
```
$ bin/glmeter
Testing https://gitlab.com for 300s
.................................................................................................................................................................................................................................................................................................................................................................................
  Responses: 369
  Best:    482ms
  Worst:   5857ms
  Average: 812ms

  Latency Distribution
    50%    765ms
    75%    819ms
    90%    874ms
    99%    1741ms
```

## Gotchas & TODO
This is very optimistic take on HTTP response times, instrumentation and slightly simplified script that could be improved for real use.

### TODO: Functionality
- Cut DNS request, TCP open, TLS negotiation: measure only HTTP response time (?)
  - focus more on user experience vs app performance metric (?)
- ~~Report 95th and 99th percentiles, instead of mean time (!)~~
- Avoid linear requests execution in favor of asynchronous (fan-out new req every 1s or so) (!)
  - otherwise stats reporting are unreliable when the longest responses have the same vote power as the fastest responses
- Report HTTP failures/timeouts
- Improve error reporting
- CLI: optparsing, use stdout/stderr, signal handling
- Set custom timeout and URL via CLI
- Add debug messages and `--verbose` mode
- Get rid of `Timeout`
- Use monotonic clock for measuring response time
- Follow the HTTP redirects?

### TODO: Organization
- Automated CI
- Add Linting
- Unit Tests
- Integration Tests
- Multiple Ruby versions build matrix
